//
//  ViewController.swift
//  notifyingtestdemo
//
//  Created by James Cash on 09-04-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var secondsField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func startTimer(_ sender: UIButton) {
        guard let txt = secondsField.text else { return }
        guard let seconds = Double(txt) else { return }

        print("time interval \(seconds)")

        let content = UNMutableNotificationContent()
        content.title = "Ready!"
        content.body = "Timer has fired"
        content.sound = UNNotificationSound.default()


        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: seconds, repeats: false)
        let request = UNNotificationRequest(identifier: "timerRequest", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)  { error in
            if let error = error {
                print("Error adding notification \(error)")
                return
            }
            print("Notification added")
        }
    }

}

